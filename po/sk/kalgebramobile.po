# translation of kalgebramobile.po to Slovak
# Roman Paholík <wizzardsk@gmail.com>, 2018.
# Matej Mrenica <matejm98mthw@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kalgebramobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-01 00:41+0000\n"
"PO-Revision-Date: 2019-08-11 16:35+0200\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.07.90\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: content/ui/About.qml:27
#, kde-format
msgid "KAlgebra Mobile"
msgstr "KAlgebra Mobile"

#: content/ui/About.qml:30
#, kde-format
msgid "A simple scientific calculator"
msgstr "Jednoduchá vedecká kalkulačka"

#: content/ui/Console.qml:31 content/ui/main.qml:65
#, fuzzy, kde-format
#| msgid "Calculate..."
msgid "Calculator"
msgstr "Spočítať..."

#: content/ui/Console.qml:49 content/ui/main.qml:91
#: content/ui/VariablesView.qml:27
#, kde-format
msgid "Variables"
msgstr ""

#: content/ui/Console.qml:67
#, fuzzy, kde-format
#| msgid "Load Script..."
msgid "Load Script"
msgstr "Načítať skript..."

#: content/ui/Console.qml:71 content/ui/Console.qml:81
#, kde-format
msgid "Script (*.kal)"
msgstr "Skript (*.kal)"

#: content/ui/Console.qml:77
#, fuzzy, kde-format
#| msgid "Save Script..."
msgid "Save Script"
msgstr "Uložiť skript..."

#: content/ui/Console.qml:88
#, fuzzy, kde-format
#| msgid "Export Log..."
msgid "Export Log"
msgstr "Exportovať záznam..."

#: content/ui/Console.qml:92
#, kde-format
msgid "HTML (*.html)"
msgstr "HTML (*.html)"

#: content/ui/Console.qml:99
#, fuzzy, kde-format
#| msgid "Evaluate..."
msgid "Evaluate"
msgstr "Vyhodnotiť..."

#: content/ui/Console.qml:99
#, fuzzy, kde-format
#| msgid "Calculate..."
msgid "Calculate"
msgstr "Spočítať..."

#: content/ui/Console.qml:105
#, kde-format
msgid "Clear Log"
msgstr "Vyčistiť záznam"

#: content/ui/Console.qml:127
#, kde-format
msgid "2D Plot"
msgstr "2D vykreslenie"

#: content/ui/Console.qml:135
#, kde-format
msgid "3D Plot"
msgstr "3D vykreslenie"

#: content/ui/Console.qml:143
#, kde-format
msgid "Copy \"%1\""
msgstr "Kopírovať \"%1\""

#: content/ui/controls/Add2DDialog.qml:51
#: content/ui/controls/Add3DDialog.qml:55
#, kde-format
msgid "Clear All"
msgstr "Vyčistiť všetko"

#: content/ui/controls/ExpressionInput.qml:72
#, kde-format
msgid "Expression to calculate..."
msgstr "Výraz na vypočítanie..."

#: content/ui/Dictionary.qml:43
#, kde-format
msgid "Name:"
msgstr ""

#: content/ui/Dictionary.qml:56 content/ui/Dictionary.qml:60
#: content/ui/Dictionary.qml:64 content/ui/Dictionary.qml:68
#, kde-format
msgid "%1:"
msgstr ""

#: content/ui/main.qml:55
#, fuzzy, kde-format
#| msgid "KAlgebra Mobile"
msgid "KAlgebra"
msgstr "KAlgebra Mobile"

#: content/ui/main.qml:72
#, kde-format
msgid "Graph 2D"
msgstr ""

#: content/ui/main.qml:79
#, kde-format
msgid "Graph 3D"
msgstr ""

#: content/ui/main.qml:85
#, kde-format
msgid "Value Tables"
msgstr ""

#: content/ui/main.qml:97
#, kde-format
msgid "Dictionary"
msgstr ""

#: content/ui/main.qml:103
#, kde-format
msgid "About KAlgebra"
msgstr ""

#: content/ui/Plot2D.qml:46 content/ui/Plot3D.qml:45
#, fuzzy, kde-format
#| msgid "Save..."
msgid "Save"
msgstr "Uložiť..."

#: content/ui/Plot2D.qml:59
#, kde-format
msgid "View Grid"
msgstr "Zobraziť mriežku"

#: content/ui/Plot2D.qml:65 content/ui/Plot3D.qml:58
#, kde-format
msgid "Reset Viewport"
msgstr "Obnoviť zobrazenie"

#: content/ui/TableResultPage.qml:11
#, kde-format
msgid "Results"
msgstr "Výsledky"

#: content/ui/Tables.qml:27
#, kde-format
msgid "Value tables"
msgstr ""

#: content/ui/Tables.qml:45
#, kde-format
msgid "Errors: The step cannot be 0"
msgstr "Chyby: krok nemôže byť 0"

#: content/ui/Tables.qml:47
#, kde-format
msgid "Errors: The start and end are the same"
msgstr "Chyby: Začiatok a koniec nemôže byť ten istý"

#: content/ui/Tables.qml:49
#, kde-format
msgid "Errors: %1"
msgstr "Chyby: %1"

#: content/ui/Tables.qml:84
#, fuzzy, kde-format
#| msgid "Input:"
msgid "Input"
msgstr "Vstup:"

#: content/ui/Tables.qml:93
#, kde-format
msgid "From:"
msgstr "Od:"

#: content/ui/Tables.qml:100
#, kde-format
msgid "To:"
msgstr "Pre:"

#: content/ui/Tables.qml:107
#, fuzzy, kde-format
#| msgid "Step:"
msgid "Step"
msgstr "Krok:"

#: content/ui/Tables.qml:113
#, kde-format
msgid "Run"
msgstr "Spustiť"

#: main.cpp:52
#, kde-format
msgid "A portable calculator"
msgstr "Prenosná kalkulačka"

#: main.cpp:53
#, fuzzy, kde-format
#| msgid "(C) 2006-2018 Aleix Pol i Gonzalez"
msgid "(C) 2006-2020 Aleix Pol i Gonzalez"
msgstr "(C) 2006-2018 Aleix Pol i Gonzalez"

#: main.cpp:54
#, fuzzy, kde-format
#| msgid "(C) 2006-2018 Aleix Pol i Gonzalez"
msgid "Aleix Pol i Gonzalez"
msgstr "(C) 2006-2018 Aleix Pol i Gonzalez"

#: main.cpp:55
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík"

#: main.cpp:55
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com"

#~ msgid "Results:"
#~ msgstr "Výsledky:"
