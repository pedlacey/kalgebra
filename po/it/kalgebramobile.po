# translation of kalgebramobile.po to Italian
# This file is distributed under the same license as the kalgebra package.
# Pino Toscano <toscano.pino@tiscali.it>, 2018, 2019, 2020, 2021.
# Paolo Zamponi <zapaolo@email.it>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kalgebramobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-01 00:41+0000\n"
"PO-Revision-Date: 2022-07-08 17:56+0200\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.2\n"

#: content/ui/About.qml:27
#, kde-format
msgid "KAlgebra Mobile"
msgstr "KAlgebra Mobile"

#: content/ui/About.qml:30
#, kde-format
msgid "A simple scientific calculator"
msgstr "Una semplice calcolatrice scientifica"

#: content/ui/Console.qml:31 content/ui/main.qml:65
#, kde-format
msgid "Calculator"
msgstr "Calcolatrice"

#: content/ui/Console.qml:49 content/ui/main.qml:91
#: content/ui/VariablesView.qml:27
#, kde-format
msgid "Variables"
msgstr "Variabili"

#: content/ui/Console.qml:67
#, kde-format
msgid "Load Script"
msgstr "Carica script"

#: content/ui/Console.qml:71 content/ui/Console.qml:81
#, kde-format
msgid "Script (*.kal)"
msgstr "Script (*.kal)"

#: content/ui/Console.qml:77
#, kde-format
msgid "Save Script"
msgstr "Salva script"

#: content/ui/Console.qml:88
#, kde-format
msgid "Export Log"
msgstr "Esporta registro"

#: content/ui/Console.qml:92
#, kde-format
msgid "HTML (*.html)"
msgstr "HTML (*.html)"

#: content/ui/Console.qml:99
#, kde-format
msgid "Evaluate"
msgstr "Vàluta"

#: content/ui/Console.qml:99
#, kde-format
msgid "Calculate"
msgstr "Calcola"

#: content/ui/Console.qml:105
#, kde-format
msgid "Clear Log"
msgstr "Cancella log"

#: content/ui/Console.qml:127
#, kde-format
msgid "2D Plot"
msgstr "Grafico 2D"

#: content/ui/Console.qml:135
#, kde-format
msgid "3D Plot"
msgstr "Grafico 3D"

#: content/ui/Console.qml:143
#, kde-format
msgid "Copy \"%1\""
msgstr "Copia \"%1\""

#: content/ui/controls/Add2DDialog.qml:51
#: content/ui/controls/Add3DDialog.qml:55
#, kde-format
msgid "Clear All"
msgstr "Cancella tutto"

#: content/ui/controls/ExpressionInput.qml:72
#, kde-format
msgid "Expression to calculate..."
msgstr "Espressione da calcolare..."

#: content/ui/Dictionary.qml:43
#, kde-format
msgid "Name:"
msgstr "Nome:"

#: content/ui/Dictionary.qml:56 content/ui/Dictionary.qml:60
#: content/ui/Dictionary.qml:64 content/ui/Dictionary.qml:68
#, kde-format
msgid "%1:"
msgstr "%1:"

#: content/ui/main.qml:55
#, kde-format
msgid "KAlgebra"
msgstr "KAlgebra"

#: content/ui/main.qml:72
#, kde-format
msgid "Graph 2D"
msgstr "Grafico 2D"

#: content/ui/main.qml:79
#, kde-format
msgid "Graph 3D"
msgstr "Grafico 3D"

#: content/ui/main.qml:85
#, kde-format
msgid "Value Tables"
msgstr "Tabelle di valori"

#: content/ui/main.qml:97
#, kde-format
msgid "Dictionary"
msgstr "Dizionario"

#: content/ui/main.qml:103
#, kde-format
msgid "About KAlgebra"
msgstr "Informazioni su KAlgebra"

#: content/ui/Plot2D.qml:46 content/ui/Plot3D.qml:45
#, kde-format
msgid "Save"
msgstr "Salva"

#: content/ui/Plot2D.qml:59
#, kde-format
msgid "View Grid"
msgstr "Mostra griglia"

#: content/ui/Plot2D.qml:65 content/ui/Plot3D.qml:58
#, kde-format
msgid "Reset Viewport"
msgstr "Ripristina area di visualizzazione"

#: content/ui/TableResultPage.qml:11
#, kde-format
msgid "Results"
msgstr "Risultati"

#: content/ui/Tables.qml:27
#, kde-format
msgid "Value tables"
msgstr "Tabelle di valori"

#: content/ui/Tables.qml:45
#, kde-format
msgid "Errors: The step cannot be 0"
msgstr "Errori: il passo non può essere 0"

#: content/ui/Tables.qml:47
#, kde-format
msgid "Errors: The start and end are the same"
msgstr "Errori: l'inizio e la fine sono gli stessi"

#: content/ui/Tables.qml:49
#, kde-format
msgid "Errors: %1"
msgstr "Errori: %1"

#: content/ui/Tables.qml:84
#, kde-format
msgid "Input"
msgstr "Ingresso"

#: content/ui/Tables.qml:93
#, kde-format
msgid "From:"
msgstr "Da:"

#: content/ui/Tables.qml:100
#, kde-format
msgid "To:"
msgstr "A:"

#: content/ui/Tables.qml:107
#, kde-format
msgid "Step"
msgstr "Passo"

#: content/ui/Tables.qml:113
#, kde-format
msgid "Run"
msgstr "Esegui"

#: main.cpp:52
#, kde-format
msgid "A portable calculator"
msgstr "Una calcolatrice portabile"

#: main.cpp:53
#, kde-format
msgid "(C) 2006-2020 Aleix Pol i Gonzalez"
msgstr "(C) 2006-2020 Aleix Pol i Gonzalez"

#: main.cpp:54
#, kde-format
msgid "Aleix Pol i Gonzalez"
msgstr "Aleix Pol i Gonzalez"

#: main.cpp:55
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Pino Toscano"

#: main.cpp:55
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "toscano.pino@tiscali.it"

#~ msgid "Results:"
#~ msgstr "Risultati:"

#~ msgid ""
#~ "In case you want to learn more about KAlgebra, you can find more "
#~ "information <a href='https://edu.kde.org/applications/mathematics/"
#~ "kalgebra/'>in the official site</a> and in the <a href='https://userbase."
#~ "kde.org/KAlgebra'>users wiki</a>.<br/>If you have any problem with your "
#~ "software, please report it to <a href='https://bugs.kde.org'>our bug "
#~ "tracker</a>."
#~ msgstr ""
#~ "Se vuoi sapere di più su KAlgebra, puoi trovare ulteriori informazioni <a "
#~ "href='https://edu.kde.org/applications/mathematics/kalgebra/'>nel sito "
#~ "ufficiale</a> e nel <a href='https://userbase.kde.org/KAlgebra'>wiki per "
#~ "gli utenti</a>.<br/>Nel caso di problemi con il software, segnalali nel "
#~ "<a href='https://bugs.kde.org'>nostro sistema di segnalazione bug</a>."
